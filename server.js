const express = require("express");
const rp = require('request-promise');
const app = express();
const cors = require('cors');

app.use(cors());

app.get('/api/question1/:countryName', (req, res) => {
    try {
        const countryName = req.params.countryName;
        url = `https://restcountries.eu/rest/v2/name/${countryName}?fullText=true`;
        options = {
            uri: url,
            headers: {
                "User-Agent": "YobetitTest",
                "Access-Control-Allow-Origin": "*"
            }
        }
        return new Promise((resolve, reject) => {
            rp(options)
                .then((response) => {
                    const result = JSON.parse(response);
                    // console.log(result,"result");
                    result.map((item) => {
                        console.log(item, 'checktheFUCKING ITEM')
                        const output = {
                            countryname: item.name,
                            region: item.region,
                            subregion: item.subregion,
                            nationalities: item.demonym,
                            flagUrl: item.flag
                        }
                        console.log(output)
                        // [{name:item.name},{region:item.region},{subregion:item.subregion}]
                        res.send(output);
                        resolve(item.name);
                    })

                })
                .catch((errorMessage) => {
                    console.log(errorMessage);
                    reject(errorMessage);
                })
        })
    }
    catch (e) {
        console.log(e, "errorMessage");
    }
})
app.get('/api/question2/:countryCode', (req, res) => {
    try {
        let countryCode = req.params.countryCode;
        let countryCodeCheck;
        // let countryCode = req.params.countryCode;
        let countryListArray = [];
        // countryListArray.push(countryCode);
        // countryCodeCheck = countryListArray.join(";");
        // console.log(countryCodeCheck,"countryCodeCheck in server")

        url = `https://restcountries.eu/rest/v2/name/${countryCode}`;
        // console.log(url,"checkthe url server")
        options = {
            uri: url,
            headers: {
                "User-Agent": "YobetitTest",
                "Access-Control-Allow-Origin": "*"
            }
        }
        return new Promise((resolve, reject) => {
            rp(options)
                .then((response) => {
                    const result = JSON.parse(response);
                    // console.log(result, "result in server");
                    let output = [];
                    result && result.map((item) => {
                        output.push({
                            name: item.name
                        })
                    })

                    console.log(output, "check the output")
                    res.send(output);
                    resolve(output)

                })
                .catch((errorMessage) => {
                    console.log(errorMessage);
                    reject(errorMessage);
                })
        })
    }
    catch (e) {
        console.log(e, "errorMessage");
    }
})

app.get('/api/question3', (req, res) => {

    try {
        url = "https://restcountries.eu/rest/v2/all?fields=name";
        options = {
            uri: url,
            headers: {
                "User-Agent": "YobetitTest",
                "Access-Control-Allow-Origin": "*"
            }
        }
        return new Promise((resolve, reject) => {
            rp(options)
                .then((response) => {
                    const result = JSON.parse(response);
                    console.log(result, "result");
                    res.send(result);
                    resolve(result)
                    // result.map((item) => {
                    //     console.log(item.name)
                    //     res.send(item.name);
                    //     resolve(item.name);
                    // })

                })
                .catch((errorMessage) => {
                    console.log(errorMessage);
                    reject(errorMessage);
                })
        })
    }
    catch (e) {
        console.log(e, "errorMessage");
    }

})

app.get('/api/question4/:credit', (req, res) => {

    let coins = parseInt(req.params.credit)
    const Reel1 = ["cherry", "lemon", "apple", "lemon", "banana", "banana", "lemon", "lemon"];
    const Reel2 = ["lemon", "apple", "lemon", "lemon", "cherry", "apple", "banana", "lemon"];
    const Reel3 = ["lemon", "apple", "lemon", "lemon", "cherry", "apple", "banana", "lemon"];


    rndNumber1 = Math.floor((Math.random() * 8));
    rndNumber2 = Math.floor((Math.random() * 8));
    rndNumber3 = Math.floor((Math.random() * 8));


    // let coins = 20;
    // let spin = coins - 1;

    const firstReelSpin = Reel1[rndNumber1];
    const secondReelSpin = Reel2[rndNumber2];
    const thirdReelSpin = Reel3[rndNumber3];
    const result = `${firstReelSpin} ${secondReelSpin} ${thirdReelSpin}`
    let amountWin =0;
    // CHECKING FOR 3 IN ROW
    try {
        if (coins >= 0) {
            if (firstReelSpin == "cherry" && firstReelSpin.includes(secondReelSpin) && secondReelSpin.includes(thirdReelSpin)) {
                coins += 50;
                amountWin = 50;
                console.log(coins, "HERE");
            }
            else if (firstReelSpin == "apple" && firstReelSpin.includes(secondReelSpin) && secondReelSpin.includes(thirdReelSpin)) {
                coins += 20;
                amountWin = 20;
                console.log(coins, "HERE");
            }
            else if (firstReelSpin == "banana" && firstReelSpin.includes(secondReelSpin) && secondReelSpin.includes(thirdReelSpin)) {
                coins += 20;
                amountWin = 20;
                console.log(coins, "HERE");
            }
            else if (firstReelSpin == "lemon" && firstReelSpin.includes(secondReelSpin) && secondReelSpin.includes(thirdReelSpin)) {
                coins += 3;
                amountWin = 3;
                console.log(coins, "HERE");
            }
            //CHECKING FOR 2 IN ROW
            else if (firstReelSpin == "cherry" && firstReelSpin.includes(secondReelSpin)) {
                coins += 40;
                amountWin = 40;
                console.log(coins, "HERE");
            }
            else if (firstReelSpin == "apple" && firstReelSpin.includes(secondReelSpin)) {
                coins += 10;
                amountWin = 10;
                console.log(coins, "HERE");
            }
            else if (firstReelSpin == "banana" && firstReelSpin.includes(secondReelSpin)) {
                coins += 5;
                amountWin = 5;
                console.log(coins, "HERE");
            }
            else {
                coins -= 1;
                console.log(coins, "CHECK THE BALANC");
            }
        }
        else {
            res.send("NOT ENOUGH MONEY");
        }

    }
    catch (e) {
        console.log(error)
    }

    // console.log(result, "CHECK THE RESULT");

    const output = {
        balance: coins,
        spinResult: result,
        firstReelSpin,
        secondReelSpin,
        thirdReelSpin,
        amountWin
    }
    res.send(output)
    // return result;


})

const port = 5000;
app.listen(port,()=>{console.log(`Server Started on Port ${port}`)});

